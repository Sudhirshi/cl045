#include <stdio.h>
int main() {
    char s[] = "Programming is fun";
    int i;

    for (i = 0; s[i] != '\0'; ++i);
    
    printf("Length of the string: %d", i);
    return 0;
}


#include <stdio.h> 
using namespace std
void findIndex(int a[], int n, int key) 
{ 
    int start = -1;
    for (int i = 0; i < n; i++) { 
        if (a[i] == key) { 
            start = i; 
            break; 
        } 
    } 
  	if (start == -1) { 
        cout << "Key not present in array"; 
        return; 
    } 
    int end = start; 
    for (int i = n - 1; i >= start; i--) { 
        if (a[i] == key) { 
            end = i; 
            break; 
        } 
    } 
    if (start == end) 
        cout << "Only one key is present at index : "
             << start; 
    else { 
        cout << "Start index: " << start; 
        cout << "nLast index: " << end; 
    } 
}
int main() 
{ 
    int a[] = { 1, 2, 7, 8, 8, 9, 8, 0, 0, 0, 8 }; 
    int n = sizeof(a) / sizeof(a[0]);  
    int key = 8;  
    findIndex(a, n, key); 
	return 0; 
} 




#include <stdio.h>
int main()
{
  int array[100], search, c, n;

  printf("Enter number of elements in array\n");
  scanf("%d", &n);

  printf("Enter %d integer(s)\n", n);

  for (c = 0; c < n; c++)
    scanf("%d", &array[c]);

  printf("Enter a number to search\n");
  scanf("%d", &search);

  for (c = 0; c < n; c++)
  {
    if (array[c] == search)
    {
      printf("%d is present at location %d.\n", search, c+1);
      break;
    }
  }
  if (c == n)
    printf("%d isn't present in the array.\n", search);

  return 0;
}

