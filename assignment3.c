//Enter your #include <stdio.h>

int main()
{
   int first, second, add, subtract, multiply;
   float divide;
 
   printf("Enter two integers\n");
   scanf("%d%d", &first, &second);
 
   add = first + second;
   subtract = first - second;
   multiply = first * second;
   divide = first / (float)second;   //typecasting
   printf("Sum = %d\n", add);
   printf("Difference = %d\n", subtract);
   printf("Multiplication = %d\n", multiply);
   printf("Division = %.2f\n", divide);
 
   return 0;
}
#include<stdio.h>
 
int main() {
   float radius, area;
 
   printf("\nEnter the radius of Circle : ");
   scanf("%d", &radius);
 
   area = 3.14 * radius * radius;
   printf("\nArea of Circle : %f", area);
 
   return (0);
}
#include<conio.h>
 
int main() {
   int length, breadth, area;
 
   printf("\nEnter the Length of Rectangle : ");
   scanf("%d", &length);
 
   printf("\nEnter the Breadth of Rectangle : ");
   scanf("%d", &breadth);
 
   area = length * breadth;
   printf("\nArea of Rectangle : %d", area);
 
   return (0);
}